{% assign postArray = nil %}
{% content_for article, class: 'article-show' %}

    <div class="experience-show --specialist">
        <div class="post-header --padding-title --black">
            {% include "includes/breadcrumbs", class: '--black' %}
            <div class="post-header__inner plate--container --inset">
                <div class="post-header__content">

                    <div class="title-intro --inset">
                        <div class="title-intro__info fl-container fl-row ai-center">
                            <div class="title-intro__info__categories">
                                {% for cat in post.categories %}
                                    <div class="category fade-in">
                                        <p>{{ cat.title }}</p>
                                    </div>
                                {% endfor %}
                            </div>
                            <div class="title-intro__info__date fade-in">
                                <p>
                                    {{ post.datum | date: "%d" }}&nbsp;
                                    {%- assign month = post.datum | date: "%B" -%}
                                    {%- case month -%}
                                    {%- when "January" -%} januari
                                    {%- when "February" -%} februari
                                    {%- when "March" -%} maart
                                    {%- when "April" -%} april
                                    {%- when "May" -%} mei
                                    {%- when "June" -%} juni
                                    {%- when "July" -%} juli
                                    {%- when "August" -%} augustus
                                    {%- when "September" -%} september
                                    {%- when "October" -%} oktober
                                    {%- when "November" -%} november
                                    {%- when "December" -%} december
                                    {%- endcase -%}
                                    {{ post.datum | date: " %Y" }}
                                </p>
                            </div>
                        </div>
                        <div class="title-intro__title title-in">
                            {% if post.titel != blank %}
                                {% assign articleTitel = post.titel %}
                            {% else %}
                                {% assign articleTitel = post.title %}
                            {% endif %}
                            {% include "includes/word_reveal", titel: articleTitel %}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="experience-show__image plate--container">
            <div class="experience-show__image__inner">
                {%- include 'includes/image/image',
                    imageSource: post.afbeelding,
                    imageLink: image.link,
                    imageAlt: post.afbeelding.title,
                    imageClass: 'width-cover scale-out',
                    imageSizes: '$-12-56%|md-12-56%',
                    imageMode: 'crop',
                    imageBoxed: false,
                    imageLazyload: true,
                    imagePng: false,
                -%}
            </div>
        </div>
        <div class="experience-show__content plate--container --inset">
            <div class="experience-show__content__inner">
                <div class="render-content fade-in">
                    {% render_content %}
                </div>
                <div class="socials">
                    <div class="socials__item">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{request.url | remove: 'https://' | remove: '://' }}" title="Deel op Facebook" class="icon-social icon-facebook" rel="noreferrer"></a>
                    </div>
                    <div class="socials__item">
                        <a target="_blank" href="https://twitter.com/intent/tweet?url={{request.url | remove: 'https://' | remove: '://' }}" title="Deel op Twitter" class="icon-social icon-twitter" rel="noreferrer"></a>
                    </div>
                    <div class="socials__item">
                        <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{request.url | remove: 'https://' | remove: '://' }}&title={{post.title}}" class="icon-social icon-linkedin" rel="noreferrer"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="related-items items-container plate--container">
            {% assign topicals = site.articles | sort_natural: 'datum' | reverse %}
            {% for topical in topicals %}
                {% unless request.path == topical.url %}
                    {% assign postArray = postArray | push: topical %}
                {% endunless %}
            {% endfor %}
            <div class="items-container__content">
                <div class="title-wrapper">
                    <div class="title-wrapper__subtitle">
                        <p>Lees verder</p>
                    </div>
                    <div class="title-wrapper__title spacer-3">
                        <h2 class="white">Meer berichten</h2>
                    </div>
                </div>
            </div>
            <div class="items-container__wrapper">
                {% for item in postArray, limit: 3 %}
                    {% include "includes/article_item" %}
                {% endfor %}
            </div>
        </div>
    </div>

{% endcontent_for %}

