import '../../config/node_modules/regenerator-runtime/runtime';
import './scripts/scrollContainer';
import './scripts/functions';
import './scripts/lazyload';
import './scripts/swiper';
import { playVideo } from './scripts/video';
playVideo();
import { imageSequence } from './scripts/imageSequence';
imageSequence();
import './scripts/faq';
import './scripts/tabs';
import './scripts/dealers'
import {general} from './scripts/general'
general();
import './scripts/updatecss';
// import { getFile } from './scripts/getFile';
// getFile();
import { fancyboxInstance } from './scripts/fancyboxInstance';
fancyboxInstance();

import './styles/style';



