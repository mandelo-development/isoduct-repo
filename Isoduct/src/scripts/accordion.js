async function accordion(thisContainer) {
  var accordionElement = thisContainer.querySelectorAll('.accordion');
  // console.log(accordionElement)
  if (typeof(accordionElement) === 'undefined' || accordionElement === null) return;
  
  accordionElement.forEach((accordionSingle) => {
    on(accordionSingle, 'click', '.accordion-item__header', function(e) {
      let t = accordionSingle.querySelectorAll(".accordion-item__body"),
      o = e.target.nextElementSibling,
      accordion = accordionSingle.querySelectorAll(".accordion-item__header");
      // console.log('accordion', t,o,accordion)
          t.forEach(e => {
              e == o ? o.style.maxHeight ? o.style.maxHeight = null : o.style.maxHeight = o.scrollHeight + "px" : e.style.maxHeight = null
          }), accordion.forEach(t => {
              t == e.target ? t.classList.toggle("active") : t.classList.remove("active")
          });
  
          if (e.target.classList.contains('active')) {
              e.target.nextElementSibling.style.maxHeight = e.target.nextElementSibling.scrollHeight + "px";
          }
      });
  })

  function on(elSelector, eventName, selector, fn) {
      var element = elSelector;
      
      element.addEventListener(eventName, function(event) {
          var possibleTargets = element.querySelectorAll(selector);
          var target = event.target;
  
          for (var i = 0, l = possibleTargets.length; i < l; i++) {
              var el = target;
              var p = possibleTargets[i];
  
              while(el && el !== element) {
                  if (el === p) {
                      return fn.call(p, event);
                  }
  
                  el = el.parentNode;
              }
          }
      });
  }
}

export {
  accordion
}