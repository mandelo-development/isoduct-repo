import gsap from '../../../../config/node_modules/gsap';

gsap.config({nullTargetWarn: false});
var contentTl = gsap.timeline();


function contentAnimationIn(fillViewportContainer) {
	var navigation;
	var breadcrumb;

	if(fillViewportContainer) {
		navigation = fillViewportContainer.querySelector('.navbar')
		breadcrumb = fillViewportContainer.querySelector('.breadcrumbs');
	} else {
		navigation = document.querySelector('.navbar');
		breadcrumb = document.querySelector('.breadcrumbs');
	}
	
	var words = document.querySelectorAll('.title-in:not(.experience-slider__item__quote) .word span');
	var wordsArray = [].slice.call(words);
	var experience_name = document.querySelectorAll('.title-quote .experience-name');
	var text = document.querySelectorAll('.fade-in');
	var socials = document.querySelectorAll('.socials__item');
	var button = document.querySelectorAll('.title-intro__button, .home-header__button');
	
	if (typeof navigation !== 'null' && navigation != null) {
		contentTl.to(navigation, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
		});
	}
	contentTl.addLabel("startAnimation", "<")
	if (typeof breadcrumb !== 'null' && breadcrumb != null) {
		var breadcrumbItems = breadcrumb.children;
		contentTl.to(breadcrumbItems, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
			stagger: 0.1,
		}, 'startAnimation');
	}
	if (typeof experience_name !== 'null' && experience_name != null) {
		contentTl.to(experience_name, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
		}, 'startAnimation');
	}
	contentTl.to(wordsArray, {
		duration: 0,
		y: "0",
		opacity: "1",
		ease: "Power4.easeOut",
		stagger: 0.01,
	}, 'startAnimation');
	if (typeof text !== 'null' && text != null) {
		contentTl.to(text, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
			stagger: 0.2,
		}, 'startAnimation');
	}
	if (typeof button !== 'null' && button != null) {
		contentTl.to(button, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
		}, 'startAnimation');
	}
	if (typeof socials !== 'null' && socials != null) {
		contentTl.to(socials, {
			duration: 0,
			opacity: "1",
			ease: "Power3.easeInOut",
			stagger: 0.2,
		}, 'startAnimation');
	}
}

export default contentAnimationIn
