import gsap from '../../../../config/node_modules/gsap';
import { contentAnimationSlideOut, contentAnimationIn } from './animations';

const root = document.documentElement;
const body = document.body;
const experienceItems = document.querySelectorAll('.index-experience-slider__item');
const fillViewportElement = document.querySelector('.fillviewport');

export const gsapTimeline = gsap.timeline();

function fillViewportIn(doneFilling, clickedElement) {

	var name = clickedElement.parentNode.querySelector('.index-experience-slider__item__name p');
	var quote = clickedElement.parentNode.querySelector('.index-experience-slider__item__quote h5');
	var date = clickedElement.parentNode.querySelector('.index-experience-slider__item__date p');

	getStyle(clickedElement.parentNode, fillViewportElement);

	function getStyle(clickedElement, viewportElement) {

		gsapTimeline.to([name, date], {
			duration: 0.8,
			opacity: "0",
		});
		gsapTimeline.to(quote, {
			duration: 0.8,
			y: "110",
			opacity: "0",
		}, '-=0.6');

		var clone = clickedElement.cloneNode(false);
	
		var fromElement = calculatePosition(clickedElement);
		var toElement = calculatePosition(viewportElement);
	
		gsapTimeline.set([clickedElement, viewportElement], { visibility: "hidden" });
		gsapTimeline.set(clone, { position: "absolute", margin: 0, zIndex: 100 });

		body.appendChild(clone);
		
		var style = {
			x: toElement.left - fromElement.left,
			y: toElement.top - fromElement.top,
			width: toElement.width,
			height: toElement.height,
			autoRound: false,
			ease: "Expo.easeInOut",
			duration: 1,
			onComplete: onComplete,
		};
	
		gsapTimeline.set(clone, fromElement, '-=0.3');
		gsapTimeline.to(clone, style, '-=0.2');
	
		function onComplete() {
			gsap.set(viewportElement, { visibility: "hidden" });
			body.removeChild(clone);
			doneFilling();
		}
	}

	function calculatePosition(element) {
		var rect = element.getBoundingClientRect();
	
		var scrollTop = window.pageYOffset || root.scrollTop || body.scrollTop || 0;
		var scrollLeft = window.pageXOffset || root.scrollLeft || body.scrollLeft || 0;
	
		var clientTop = root.clientTop || body.clientTop || 0;
		var clientLeft = root.clientLeft || body.clientLeft || 0;
	
		return {
		top: Math.round(rect.top + scrollTop - clientTop),
		left: Math.round(rect.left + scrollLeft - clientLeft),
		height: rect.height,
		width: rect.width };
	}
}

export default fillViewportIn