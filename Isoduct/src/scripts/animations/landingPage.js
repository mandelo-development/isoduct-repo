import gsap from '../../../../config/node_modules/gsap';
import { setCookie } from '../functions';
import { contentAnimationIn } from './animations';

export const landingPageTL = gsap.timeline();
export const landingPageParent = document.querySelector('.landing-container');
if(landingPageParent) {
    var landingPageChildren = landingPageParent.querySelectorAll('.landing-screen');
    var titleWrapper = landingPageParent.querySelector('.titles__wrapper')
    var titles = titleWrapper.querySelectorAll('.title')
    var titleHeight = titles[0].offsetHeight;
    var logo = landingPageParent.querySelector('.landing-screen__logo .logo');
}

// function landingPage() {
    
// 	landingPageTL.to(landingPageChildren[0], {
// 		duration: 1,
// 		y: "0",
//         ease: "Expo.easeInOut",
//     }); 

//     landingPageTL.to(logo, {
//         duration: 1,
//         y: 0,
//         ease: "Power4.easeOut",
//     }, "-=0.5");

//     landingPageTL.to(landingPageChildren[0], { 
// 		duration: 1,
// 		y: "-100%",
//         ease: "Expo.easeInOut",
//         delay: 0.3,
//     }); 

//     landingPageTL.to(landingPageParent, .1, {backgroundColor: "#00AECE" }, "-=0.4");
    
// 	landingPageTL.to(landingPageChildren[1], {
// 		duration: 1,
// 		y: "-100%",
//         ease: "Expo.easeInOut",
//     }, "-=1");

//     landingPageTL.to(titleWrapper, .4, {
//         y: 0,
//         ease: "Power4.easeOut",
//     }, "-=0.5");

//     landingPageTL.to(landingPageChildren[1], {
// 		duration: 1,
// 		y: "-200%",
//         ease: "Expo.easeInOut",
//         delay: 0.3,
//     });

//     landingPageTL.to(landingPageParent, .1, {backgroundColor: "#000000" }, "-=0.4");
    
// 	landingPageTL.to(landingPageChildren[2], {
// 		duration: 1,
// 		y: "-200%",
//         ease: "Expo.easeInOut",
//     }, "-=1");

//     landingPageTL.to(titleWrapper, .4, {
//         y: "-" + titleHeight,
//         ease: "Power4.easeOut",
//     }, "-=0.5");
    
// 	landingPageTL.to(landingPageChildren[2], {
// 		duration: 1,
// 		y: "-300%",
//         ease: "Expo.easeInOut",
//         delay: 0.3,
//     });

//     landingPageTL.to(landingPageParent, .1, {backgroundColor: "#FFFFFF" }, "-=0.4");
    
// 	landingPageTL.to(landingPageChildren[3], {
// 		duration: 1,
// 		y: "-300%",
//         ease: "Expo.easeInOut",
//     }, "-=1");

//     landingPageTL.to(titleWrapper, .4, {
//         y: "-" + titleHeight * 2,
//         ease: "Power4.easeOut",
//     }, "-=0.5");

//     landingPageTL.to(landingPageParent, .4, {
//         autoAlpha: 0,
//         delay: 1,
//     });

//     landingPageTL.to(landingPageParent, {
//         onComplete: () => contentAnimationIn(),
//     }, "-=0.6");
    
//     console.log('set cookie');
//     setCookie('introAnimation', true, 365, '/' );
// }

function landingPage() {
    
	landingPageTL.to(landingPageChildren[0], {
		duration: 1,
		y: "0",
        ease: "Expo.easeInOut",
    }); 

    landingPageTL.to(logo, {
        duration: 1,
        y: 0,
        ease: "Power4.easeOut",
    }, "-=0.5");
    

    // landingPageTL.to(landingPageParent, .4, {
    //     autoAlpha: 0,
    //     delay: 1,
    // });

    landingPageTL.to(landingPageParent, {
		duration: .65,
		y: "-100%",
		ease: "Expo.easeInOut",
        // delay: 1,
	});

    landingPageTL.to(landingPageParent, {
        onComplete: () => contentAnimationIn(),
    }, "-=0.6");
    
    console.log('set cookie');
    setCookie('introAnimation', true, 365, '/' );
}

export default landingPage
