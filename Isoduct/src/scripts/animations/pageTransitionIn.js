import gsap from '../../../../config/node_modules/gsap';

export const loadingScreenTL = gsap.timeline();
export const loadingScreen = document.querySelector('.loading-screen');
// export const loadingScreenLogo = document.querySelector('.loading-screen .logo');


// loadingScreenTL.to(loadingScreen, {backgroundColor: "#00AECE", duration: .65, ease: "Expo.easeInOut"});

function pageTransitionIn(done) {
	// loadingScreenTL.set(loadingScreenLogo, {y: "100%"})
	loadingScreenTL.to(loadingScreen, {
		duration: .65,
		y: "0",
        ease: "Expo.easeInOut",
		onComplete: () => done(),
	});
	// loadingScreenTL.to(loadingScreenLogo, {
	// 	duration: 1,
	// 	y: "0",
	// 	ease: "Power4.easeOut",
	// 	onComplete: () => done(),
    // }, '-=0.6');
}

export default pageTransitionIn

