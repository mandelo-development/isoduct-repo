import gsap from '../../../../config/node_modules/gsap';
import { contentAnimationIn } from './animations';
import { loadingScreenTL, loadingScreen, loadingScreenLogo } from './pageTransitionIn';

function pageTransitionOut(done) {

	
    loadingScreenTL.to(loadingScreen, {
		duration: .65,
		y: "-100%",
		ease: "Expo.easeInOut",
	});
	// loadingScreenTL.to(loadingScreenLogo, {
	// 	duration: 0.8,
	// 	y: "0",
	// 	ease: "Power4.easeOut",
	// }, "-=0.5");
	loadingScreenTL.to(loadingScreen, {
		onComplete: () => contentAnimationIn(),
	}, "-=1");

	loadingScreenTL.set(loadingScreen, { y: "100%" });
	// loadingScreenTL.set(loadingScreenLogo, { y: "100%" });
	
}

export default pageTransitionOut