var $ = require('../../../config/node_modules/jquery/dist/jquery');
import MarkerClusterer from '../../../config/node_modules/@googlemaps/markerclustererplus';

const initGmaps = () => {

    var addMarkerLatLong,
        addMarkerCluster,
        addMarkerListener,
        addMarkersToMap,
        markers = [],
        initializeDealerSelection,
        initializeMapsFooter,
        mapGraphicOptions,
        zoomToMarker,
        searchInput,
        searchInputBtn,
        useMyLocation,
        geocodeMyLocation,
        getDistance,
        calculateDistance,
        sortList,
        dealerPageClass;

    window.initMaps = function() {
        var map;
        dealerPageClass = document.querySelector('.dealer');

        window.smallIcon = {
            url: '/theme/assets/images/marker.svg',
            scaledSize: new google.maps.Size(25, 25)
        };
        window.largerIcon = {
            url: '/theme/assets/images/marker.svg',
            scaledSize: new google.maps.Size(30, 30)
        };
        window.dealerMarkers = {};
        if ($(".dealer__map #map")[0]) {
            map = initializeMapsFooter($(".dealer__map #map")[0]);
            addMarkersToMap(map, $(".dealer__item"));
            addMarkerCluster(map);
            searchInput(map);
            searchInputBtn(map);
            useMyLocation(map);
        }
    };

    initializeMapsFooter = function(container) {
        var map;
        map = new google.maps.Map(container, {
          zoom: 7,
          maxZoom: 18,
          center: {
            lat: 51.978,
            lng: 5.3859
          },
          disableDefaultUI: true,
          styles: mapGraphicOptions
        });
        return map;
    };

    zoomToMarker = function(map, marker) {
        map.setZoom(16);
        map.panTo(marker.position);
        $(".dealer__item").each(function() {
        if ($(this).data("marker")) {
            return $(this).data("marker").setIcon(smallIcon);
        }
        });
        return marker.setIcon(largerIcon);
    };

    addMarkersToMap = function(map, dealers) {
        clearOverlays();
        var bounds, geocoder;
        geocoder = new google.maps.Geocoder();
        bounds = new google.maps.LatLngBounds();

        for (var l = 0; l < dealers.length; l++) {
            (function(l){
                setTimeout(function(i){
                    geocodeAddress(map, geocoder, bounds, l, dealers[l]);

                    if (l === dealers.length) {
                        setTimeout(function() {
                            addMarkerCluster(map, markers);
                        }, 1000)
                    }
                }, l * 1);//275
                
            })(l);
        }

        return setTimeout(function() {
            return addMarkerCluster(map, markers);
        }, 2750);
    };

    function geocodeAddress (map, geocoder, bounds, index, current) {
        var marker,
            address,
            lat,
            long,
            dealerID;
            address = $(current).data("address");
            lat = parseFloat($(current).data("lat"));
            long = parseFloat($(current).data("long"));
            address = $(current).data("address");
            dealerID = $(current).data("dealer-id");

            marker = addMarkerLatLong(map, lat, long, dealerID);
            bounds.extend(marker.getPosition());
            addMarkerListener(marker, $(current), map);
            markers.push(marker);
    }

    function clearOverlays() {
        for (var i = 0; i < markers.length; i++ ) {
          markers[i].setMap(null);
        }
        markers.length = 0;
    }

    addMarkerLatLong = function(map, lat, long, id) {
        var marker;
        marker = new google.maps.Marker({
            map: map,
            position: { lat: lat, lng: long },
            icon: smallIcon,
            id: id,
        });
        
        return marker;
    };

    const ClusterIcon = color => window.btoa(`
        <svg fill="${color}" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 240 240">
            <circle cx="120" cy="120" opacity="1" r="70" />
            <circle cx="120" cy="120" opacity="1" r="95" />
        </svg>`)

    addMarkerCluster = function(map, markers) {
       return new MarkerClusterer(map, markers, {
        styles: [`white`].map(color => ({
          url: `data:image/svg+xml;base64,${ClusterIcon(color)}`,
          height: 34,
          width: 34,
          textColor: `black`,
          textSize: 14,
          anchorText: ['5','0']
        })),
      })
    };

    addMarkerListener = function(marker, htmlElement, map) {
        marker.addListener("click", function() {
            htmlElement[0].scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
            zoomToMarker(map, marker);
            return htmlElement.trigger("click");
        });
        return htmlElement.on("click", function() {
            zoomToMarker(map, marker);
            htmlElement.siblings().removeClass("active");
            htmlElement.addClass("active");
            return htmlElement[0].scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
        });
    };

    searchInput = function (map) { 
        var autocomplete;
        var input = document.getElementById('dealer-search-input');
        var options = {
            types: ['geocode'],
            componentRestrictions: {country: 'nl'}
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.setFields(["address_components", "geometry", "name"]);

        var url = new URL(window.location);
        var queryAddress = url.searchParams.get("s");

        if (queryAddress !== null) {
            setTimeout(function() {
                fillInput(input, queryAddress, 0, 100);
                input.focus();
            },200)
            
        }

        
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            if(dealerPageClass.classList.contains('dealer-page-active')) {
                dealerPageClass.classList.remove('dealer-page-active');
            }

            var place = autocomplete.getPlace();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
                getDistance(map, place.geometry.location, true);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
                getDistance(map, place.geometry.location, true);
            }
        
        });

        function fillInput(target, message, index, interval) {
            if (index < message.length) { 
              $(target).val($(target).val()+message[index++]);
              setTimeout(function () { fillInput(target, message, index, interval); }, interval);
            } 
        }
    };

    searchInputBtn = function (map) {
        var searchInputForm = document.getElementById('dealer-search-form-btn');
        searchInputForm.addEventListener("click", function(event) {
            event.preventDefault;
        });
    }

    useMyLocation = function (map) {
        var useLocationBtn = document.getElementById('use-location-btn');
        var url = new URL(window.location);
        var queryLocation = url.searchParams.get("l");

        useLocationBtn.addEventListener("click", getLocation);

        if (queryLocation !== null && queryLocation === 'myLocation') {
            setTimeout(function() {
                getLocation();
            }, 200)
            
        }

        function getLocation() {
            if(dealerPageClass.classList.contains('dealer-page-active')) {
                dealerPageClass.classList.remove('dealer-page-active');
            }
            
            // check if user's browser supports Navigator.geolocation
            if (navigator.geolocation) {
                var myLocation = {};
                navigator.geolocation.getCurrentPosition(function(position){
                    myLocation.lat = position.coords.latitude;
                    myLocation.lng = position.coords.longitude;
                    
                    geocodeMyLocation(map, myLocation)
                });
            } else {
                console.log('no support')
                // Browser doesn't support Geolocation
                handleLocationError(false, map.getCenter());
            }
            
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                console.log(browserHasGeolocation ?
                  'Error: The Geolocation service failed.' :
                  'Error: Your browser doesn\'t support geolocation.');
            }
        }
    }

    geocodeMyLocation = function (map, latLng) {
        var locationInput = document.getElementById('dealer-search-input'),
            geocoderLatLng;
            geocoderLatLng = new google.maps.Geocoder();
            geocoderLatLng.geocode({ location: latLng }, (results, status) => {
            if (status === "OK") {
                if (results[0]) {
                    var city, country, address;
                    for (var i = 0; i < results[0].address_components.length; i++) {
                        for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                            switch (results[0].address_components[i].types[b]) {
                                case 'locality':
                                    city = results[0].address_components[i].long_name;
                                    break;
                                case 'country':
                                    country = results[0].address_components[i].long_name;
                                    break;
                            }
                        }
                    }
                    address = city + ', ' + country;
                    locationInput.value = address;
                    map.setCenter(latLng);
                    getDistance(map, latLng, false);
                } else {
                    console.log("Geen resultaten gevonden");
                }
            } else if(status === "OVER_QUERY_LIMIT") {
                setTimeout(function() {
                    geocodeMyLocation(map, latLng);
                }, 275)
            } else {
                console.log("Geocoder failed due to: " + status);
            }
        });
    }

    getDistance = function (map, center, isFunction) {
        var radius = 20000;
        
        //in meters 
        markers.filter(function (marker) {
            var markerLat = marker.position.lat();
            var markerLng = marker.position.lng();
            var centerLat;
            var centerLng;

            if(isFunction) {
                centerLat = center.lat();
                centerLng = center.lng();
            } else {
                centerLat = center.lat;
                centerLng = center.lng;
            }

            var distance = calculateDistance(markerLat, markerLng, centerLat, centerLng);
            if (distance) {
                var dealerItem = $("[data-dealer-id=" + marker.id + "]");
                var distanceKm = distance / 1000;
                var distanceKmDec = distanceKm.toFixed(2);
                $(dealerItem).find('.dealer__item__distance').empty().append(distanceKmDec + ' km');
            }
        });

        sortList();
    };

    calculateDistance = function (fromLat, fromLng, toLat, toLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(fromLat, fromLng), new google.maps.LatLng(toLat, toLng));
    };

    sortList = function () {
        var parent = $('.dealer__locations__inner');
        $(".dealer__item").sort(function(a, b) {
            a = parseFloat($(".dealer__item__distance", a).text());
            b = parseFloat($(".dealer__item__distance", b).text());
            return a - b;
          }).appendTo(parent);
    }

    mapGraphicOptions = [
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "lightness": "17"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#00AECE"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#00AECE"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#FF4700"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "lightness": 17
                },
                {
                    "color": "#00AECE"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels",
            "stylers": [
                {
                    "invert_lightness": true
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#00AECE"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#007186"
                },
                {
                    "lightness": "39"
                },
                {
                    "gamma": "0.43"
                },
                {
                    "saturation": "-47"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#00AECE"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#555555"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        }
    ];
}

export {initGmaps}