async function fancyboxInstance () {

    var $ = require("../../../config/node_modules/jquery");
    window.$ = window.jQuery = $;
    require('../../../config/node_modules/@fancyapps/fancybox');

    $(document).ready(function() {
        $('[data-fancybox="gallery-element"]').fancybox();
    });
    
 } export {
    fancyboxInstance
 }