var $ = require("../../../config/node_modules/jquery");
import {scrollCont} from './scrollContainer';
import {debounce} from './functions';

const faq = () => {
    $(document).ready(function() {
      var faq = $('.faq');
      var mediaQuery = window.matchMedia('(min-width: 990px)');
      
      if ($(faq).length) {

            if (mediaQuery.matches) {
                $('.tab__item__link').click(function(clickEvent) {
                    clickEvent.preventDefault();
                    var target = $(this).attr('href');
                    scrollCont.scrollTo(target, -140);
                });
                $(document).on('click', '.search__result__anchor' , function(click){
                    click.preventDefault();
                    var questionTarget = $(this).attr('href').split('#');
                    let qt = questionTarget[questionTarget.length-1];

                    // console.log(`#${qt}`);
                    scrollCont.scrollTo(`#${qt}`, -100);
                    $("#faq-search-results").removeClass('search-active');
                });
            } else {
                $('.tab__item__link').click(function(clickEvent) {
                    clickEvent.preventDefault();
                    var target = $(this).attr('href');
                    $("html, body").animate({
                        scrollTop: $(target).offset().top - 140,
                    }, "slow");
                });
                $(document).on('click', '.search__result__anchor' , function(click){
                    click.preventDefault();
                    var questionTarget = $(this).attr('href');
                    $("html, body").animate({
                        scrollTop: $(questionTarget).offset().top - 100, 
                    }, "slow");
                    $("#faq-search-results").removeClass('search-active');
                });
            }

            var intersectionOptions = {
                root: null,
                rootMargin: '-140px 0px -40% 0px',
                threshold: 0.9,
            };
            var anchorPoints = $('.faq .anchor');

            function intersectionCallbackConts(entries, observer) {
                $(entries).each(function(i, entry) {
                    let elem = entry.target;
                    if (entry.intersectionRatio > intersectionOptions.threshold) {
                        $(anchorPoints).removeClass('active');
                        $(elem).addClass('active');

                        var id = $(elem).attr('id');
                        $('.tab__item').removeClass('active');
                        $('.tab__item[data-id=' + id + ']').addClass('active');
                    } else {}
                });
            }

            var observer = new IntersectionObserver(intersectionCallbackConts, intersectionOptions);

            $(anchorPoints).each(function(i, element) {
                observer.observe(element);
            });

            // Search faq

            var searchInput = $('#faq-search-input');

            $(searchInput).on("keyup", debounce(function(event) {
                var keyupText = this.value;
                var pageName = window.location.pathname;

                var get_url = "/zoekresultaten?q=" + keyupText + "&page=" + pageName ;

                $.ajax({url: get_url , success: function(result) {
                    $("#faq-search-results").html(result);
                    $("#faq-search-results").addClass('search-active');
                }});
            }, 500));

            $("#faq-search-results").mouseenter(function(){
                scrollCont.stop();
            }).mouseleave(function(){
                scrollCont.start();
                scrollCont.update();
            });

            $(document).on('click', function(event) {
                if (!$(event.target).closest('#faq-search-result, .faq__search').length) {
                    $("#faq-search-results").removeClass('search-active');
                }
            });

            function accordionClick() {

                var acc = document.getElementsByClassName("question__item__question");
                var i;
                var j;
        
                for (i = 0; i < acc.length; i++) {
                    acc[i].onclick = function() {
        
                        if (this.classList.contains("active")) {
        
                            this.nextElementSibling.style.maxHeight = null;
                            this.classList.remove("active");
        
                        } else {
        
                            for (j = 0; j < acc.length; j++) {
                                acc[j].nextElementSibling.style.maxHeight = null;
                                acc[j].classList.remove("active")
                            }
        
                            this.classList.add("active");
        
                            var panel = this.nextElementSibling;
        
                            if (panel.style.maxHeight) {
                                panel.style.maxHeight = null;
                            } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                            }
        
                        }
        
                    }
                }
            }

            if ($(window).width() <= 989) {
                accordionClick();
            }

        }
        
    });
}
export { 
    faq 
}