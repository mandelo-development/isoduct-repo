
import {scrollCont, barba} from './scrollContainer';
import {debounce, loadScript, throttle} from './functions';
var $ = require("../../../config/node_modules/jquery");

async function general() {
    $(document).ready( function () {

        let forms = document.querySelectorAll("form.contact-form");

        if(forms) {
            for (var i = 0; i < forms.length; i++) {
                forms[i].addEventListener('submit', function(e) {
                let $this = this;
                e.preventDefault();
        
                    executeRecaptcha($this, function(){
                        $this.submit() // Submit the form after the token is generated.
                    })
                });
            }
        }
        
        if ($(window).width() <= 989) {
            scrollCont.destroy();

            var acc = document.getElementsByClassName("dropdown-arrow");
            var i;
            var j;
    
            for (i = 0; i < acc.length; i++) {
                acc[i].onclick = function() {
    
                    if (this.classList.contains("active")) {
    
                        this.nextElementSibling.style.height = null;
                        this.classList.remove("active");
    
                    } else {
    
                        for (j = 0; j < acc.length; j++) {
                            acc[j].nextElementSibling.style.height = null;
                            acc[j].classList.remove("active")
                        }
    
                        this.classList.add("active");
    
                        var panel = this.nextElementSibling;
    
                        if (panel.style.height) {
                            panel.style.height = null;
                        } else {
                            panel.style.height = panel.scrollHeight + "px";
                        }
                    }
                }
            }

            $('.navbar-toggler').click(throttle(function() {
                var nav = $('.navigation');
                var dropdownMenu = $('.dropdown-menu');
                var dropdownArrow = $('.dropdown-arrow');
                var clicks = $(this).data('clicks');
            
                if (!clicks) {
                    $(nav).addClass('menu-open');
                    $('body').addClass('lock-scroll');
                } else {
                    $(dropdownArrow).removeClass('active');
                    $(dropdownMenu).removeAttr("style");
                    $(nav).removeClass('menu-open');
                    $('body').removeClass('lock-scroll');
                }

                $(this).data("clicks", !clicks);
            }, 1000));

            var tussenruimte = $(".tussenruimte");
    
            tussenruimte.each(function (i, element) {
                var mobileSpace = parseInt($(element).attr('data-height'));
    
                if (!isNaN(mobileSpace)) {
                    $(element).css({'height': mobileSpace + 'px'});
                }
            });

            var section = $(".section-container");

            section.each(function (i, element) {
                var mobileSpaceAbove = parseInt($(element).attr('data-boven'));
                var mobileSpaceUnder = parseInt($(element).attr('data-onder'));

                if (!isNaN(mobileSpaceAbove)) {
                    var padTop = parseInt($(element).css('padding-top'));
                    
                    $(element).css({'padding-top': mobileSpaceAbove + 'px'});
                }

                if (!isNaN(mobileSpaceUnder)) {
                    var padBot = parseInt($(element).css('padding-bottom'));
                    
                    $(element).css({'padding-bottom': mobileSpaceUnder + 'px'});
                }
            });

            $('.footer__back-to-top .button').click(function (e) { 
                e.preventDefault();
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            });

            $('a[href*=\\#]:not(.tab__item__link)').on('click', function(event){     
                event.preventDefault();
                var target = this.hash;
                var offsetTop = $(target).offset();
                $("html, body").animate({scrollTop: offsetTop.top}, "slow");
                return false;
            });

        } else {

            $('.footer__back-to-top .button').click(function (e) { 
                e.preventDefault();
                scrollCont.scrollTo(0,0);
            });

            $('a[href*=\\#]:not(.tab__item__link)').on('click', function(event){     
                event.preventDefault();
                scrollCont.scrollTo(this.hash);
                setTimeout(() => {
                    scrollCont.update();
                }, 500);
            });
        
            // Hide Navbar on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 1;
            var navbar = $('.navigation');
            var navbarHeight = $(navbar).outerHeight();

            scrollCont.on('scroll', function (event) {
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 120);

            function hasScrolled() {

                var offsetScroll = $('#js-scroll').offset().top;
                var st = Math.abs(offsetScroll);

                // Make sure they scroll more than delta
                if(Math.abs(lastScrollTop - st) <= delta)
                    return;

                if (navbarHeight > st) {
                    $(navbar).addClass('nav-top');
                    $(navbar).removeClass('fixed-header');
                } else {
                    $(navbar).removeClass('nav-top');
                    $(navbar).addClass('fixed-header');
                }

                // If they scrolled down and are past the navbar, add class .nav-down.
                // This is necessary so you never see what is "behind" the navbar.
                if (st > lastScrollTop && st > navbarHeight){
                    // Scroll Down
                    $(navbar).removeClass('nav-up').addClass('nav-down');
                } else {
                    // Scroll Up
                    if(st + $(window).height() < $(document).height()) {
                        $(navbar).removeClass('nav-down').addClass('nav-up');
                    }
                }
                
                lastScrollTop = st;
            }
        }
    });
}
async function ifEditing() {
    $(document).ready( function () {
        if(parent.document.getElementById('config-bar')) {
            $('body').addClass('config-mode');
            var siteViewer = parent.document.getElementById('site-viewer-wrapper');
            var siteViewerIframe = parent.document.getElementById('site-viewer');
            var path = barba.C.current.url.path;
            // siteViewer.setAttribute('data-editing-path', path);
            barba.destroy();
            setTimeout(() => {
                scrollCont.destroy();
            }, 100);
        }
    });
}
export {
    general, ifEditing
}