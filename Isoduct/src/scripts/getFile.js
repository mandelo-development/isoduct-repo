var $ = require("../../../config/node_modules/jquery");

const getFile = () => {
    $('.get-file').on('click', function (e) {
        e.preventDefault();

        var fileName = $(this).attr("data-file-name");
        var url = $(this).attr("href");

        console.log(fileName, url);

        let req = new XMLHttpRequest();
        req.open("GET", url, true);
        req.responseType = "blob";
        req.onload = function (event) {
            var blob = req.response;
            var link = document.createElement('a');
            link.target = '_blank';
            link.href = window.URL.createObjectURL(blob);
            console.log(link);
            link.download = fileName;
            link.click();
        };

        req.send();
    });
}
export {
    getFile
}
