import {scrollCont} from './scrollContainer';
var sequenceArray = [];
const imageSequence = () => {
    
    var containers = document.querySelectorAll(".image-sequence");
    var mediaQuery = window.matchMedia('(min-width: 990px)');

    if(containers && mediaQuery.matches && parent.document.getElementById('config-bar') === null) {
        function isInViewportTop(el) {
            var rect = el.getBoundingClientRect();
            return (
                rect.top <= 0 &&
                rect.bottom >= (window.innerHeight || document.documentElement.clientHeight)
            );
        }

        for (var i = 0; i < containers.length; i++) {
            var thisContainer = containers[i];
            var imagesPath = thisContainer.getAttribute('data-path');
            var frameCount = parseInt(thisContainer.getAttribute('data-frames'));
            var canvas = thisContainer.querySelector("canvas");

            imageSequenceInit(thisContainer, imagesPath, frameCount, canvas);
        }

        function imageSequenceInit(container, imagesPath, frameCount, canvas) {
            var images = [];
            var containerHeight = container.scrollHeight;
            var scrollTop;
            var maxScrollTop;
            var scrollFraction;
            var fixedScrollFraction;
            var frameIndex;
            var imageSequencePoints = false;
            
            for (var a = 0; a < frameCount; a++) {
                var img = new Image;
                img.src = `/theme/assets/images/${imagesPath}/${a.toString().padStart(5, "0")}.jpg`;
                images.push(img);
                sequenceArray.push(img);
            }

            if(canvas.id === "flue-image-sequence-build") {
                canvas.width = 850;
            } else {
                canvas.width = 1460;
            }
            canvas.height = 820;
            var context = canvas.getContext("2d");

            function getFrameIndex(scrolledTop, imageSequencePoints) {
                maxScrollTop = containerHeight - window.innerHeight;
                scrollFraction = scrolledTop / maxScrollTop;
                fixedScrollFraction = parseFloat(scrollFraction.toFixed(3));

                frameIndex = Math.min(
                    frameCount - 1,
                    Math.ceil(fixedScrollFraction * frameCount)
                );

                if(imageSequencePoints === true) {
                    getImageSequencePoints(fixedScrollFraction);
                }

                return frameIndex;
            }

            var updateImage = (index) => {
                if (images[index]) {
                    context.fillRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(images[index], 0, 0);
                }
            };

            function getImageSequencePoints (fixedScrollFraction) {
                var sequencePoints = container.querySelectorAll(".image-sequence__point");
                var sequencePointsParent = container.querySelector(".image-sequence__points");

                for (var s = 0; s < sequencePoints.length; s++) {
                    var $this = sequencePoints[s];
                    var dataPoint = parseFloat($this.getAttribute('data-point'));
                    
                    if (fixedScrollFraction >= dataPoint && dataPoint !== 0) {
                        $this.classList.add('active');
                        if(sequencePointsParent) {
                            sequencePointsParent.classList.add('point-' + s);
                        }
                    }

                    if (fixedScrollFraction <= dataPoint && $this.classList.contains('active') && dataPoint !== 0)  {
                        $this.classList.remove('active');
                        if(sequencePointsParent) {
                            sequencePointsParent.classList.remove('point-' + s);
                        }
                    }
                }
            }

            setTimeout(function() {
                var newLocation = null; // avoids a new 'draw' function generation
                var offsetTopContainer = Math.abs(container.getBoundingClientRect().top);

                scrollCont.on('scroll', function(scrollEvent) {
                    if(isInViewportTop(container) === true) {
                        if (container.classList.contains('has-points')) {
                            imageSequencePoints = true;
                        }
                        scrollTop = scrollEvent.scroll.y - offsetTopContainer;
                        getFrameIndex(scrollTop, imageSequencePoints);
                        
                        if (newLocation === null) { // if set, we already are waiting to draw
                            requestAnimationFrame(() => updateImage(frameIndex + 1));
                        }
                    }
                });

            }, 0);

            images[0].onload = function() {
                context.fillRect(0, 0, canvas.width, canvas.height);
                context.drawImage(images[0], 0, 0);
            };
        }
        
    }
}
export {
    imageSequence
}