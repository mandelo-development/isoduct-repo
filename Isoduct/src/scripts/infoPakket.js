import {gsap} from '../../../config/node_modules/gsap';
import { scrollCont } from "./scrollContainer";
var $ = require("../../../config/node_modules/jquery");

const infoPakket = (thisContainer) => {

    var infoPakketTrigger = $(".info-pakket-trigger");
    var infoPakket = $(".info-pakket");
    var infoPakketClose = $(".info-pakket__close");

    $(infoPakket).mouseenter(function(){
        scrollCont.stop();
    }).mouseleave(function(){
        scrollCont.start();
        scrollCont.update();
    });

    $(infoPakketTrigger).click(function() {
        $(infoPakket).addClass('opened');
        gsap.to(infoPakket, {
            xPercent: -100,
            duration: 1,
            ease: "Expo.easeInOut",
        })
    });
    $(infoPakketClose).click(function() {
        $(infoPakket).removeClass('opened');
        gsap.to(infoPakket, {
            xPercent: 0,
            duration: 1,
            ease: "Expo.easeInOut",
        })
    });
} 

export {
    infoPakket
}