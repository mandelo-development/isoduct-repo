import barba from "../../../config/node_modules/@barba/core";
import barbaPrefetch from "../../../config/node_modules/@barba/prefetch";
import LocomotiveScroll from "../../../config/node_modules/locomotive-scroll";
import {
  landingPage,
  contentAnimationIn,
  contentAnimationSlideOut,
  pageTransitionIn,
  pageTransitionOut,
  fillViewportIn,
} from "./animations/animations";
import {
  experienceCarousel,
  indexExperienceCarousel,
  tabSlider,
  uspSlider,
  logoSlider,
  fadeSlider,
} from "./swiper";
import { faq } from "./faq";
import { imageSequence } from "./imageSequence";
import { initGmaps } from "./dealers";
import { tabs } from "./tabs";
import { general, ifEditing } from "./general";
import { loadScript, getCookie } from "./functions";
import { playVideo } from "./video";
import { observeImages } from "./lazyload";
import { loadingScreen, loadingScreenTL } from "./animations/pageTransitionIn";
import { infoPakket } from "./infoPakket";
import gsap from "../../../config/node_modules/gsap";
// import { accordion } from './accordion';

let scrollCont;

barba.use(barbaPrefetch);

barba.hooks.beforeOnce((data) => {
  if (
    getCookie("introAnimation") !== "true" &&
    window.location.pathname === "/"
  ) {
    document.body.classList.add("is-homepage");
    gsap.set(document.querySelector(".landing-container"), {
      autoAlpha: 1,
      immediateRender: true,
    });
  } else {
    // gsap.set(document.querySelector('.load-container .loading-screen'), {backgroundColor: "#00AECE", immediateRender: true})
    gsap.set(document.querySelector(".landing-container"), {
      autoAlpha: 0,
      immediateRender: true,
    });
  }
});
barba.hooks.once((data) => {
  ifEditing();
  if (
    document.querySelector(".landing-container") !== null &&
    getCookie("introAnimation") !== "true"
  ) {
    landingPage();
  } else {
    // loadingScreenTL.to(loadingScreen, {backgroundColor: "#00AECE", duration: 1, ease: "Expo.easeInOut"}, "-=.4");
    pageTransitionOut("", true);
  }
});
barba.hooks.beforeEnter((data) => {
  // accordion(data.next.container);
});
barba.hooks.afterEnter((data) => {
  tabSlider(data.next.container);
  infoPakket(data.next.container);
  logoSlider(data.next.container);
  fadeSlider(data.next.container);
});

barba.hooks.enter((data) => {
  document.querySelector("body").classList.remove("lock-scroll");
  document.querySelector("body").classList.remove("is-homepage");
});
barba.hooks.after((data) => {
  scrollCont.scrollTo(0, 0);
  scrollCont.update();
  general();
  observeImages();

  // if(data.current.url.path == '/*' && data.next.url.path == '/faq') {
  if (data.current.url.href.includes("isoduct")) {
    // console.log(data.current.url.href.includes('isoduct'), data)
    let wl = data.next.url.hash;
    scrollCont.scrollTo(`#${wl}`, -100);
  }
});
barba.init({
  debug: false,
  timeout: 5000,
  views: [
    {
      namespace: "home",
      beforeEnter() {
        experienceCarousel();
        imageSequence();
        uspSlider();
      },
    },
    {
      namespace: "experience-index",
      beforeEnter() {
        indexExperienceCarousel();
      },
    },
    {
      namespace: "page-show",
      beforeEnter() {
        imageSequence();
        faq();
        tabs();
        playVideo();
      },
    },
    {
      namespace: "dealer-index",
      beforeEnter({ next }) {
        if (typeof window.initMaps === "function") {
          // console.log('is function');
          window.initMaps();
        } else {
          // console.log('no function');
          loadScript(
            next.container,
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyCBQ4KNliLDsmEKJG3PWw2SfcoPPoLQ9OE&libraries=geometry,places&callback=initMaps",
            initGmaps()
          );
        }
      },
    },
  ],
  transitions: [
    {
      name: "page",
      sync: false,
      once({ next }) {
        // init LocomotiveScroll on page load
        smooth(next.container);
      },
      leave({ current }) {
        const done = this.async();
        pageTransitionIn(done);
      },
      beforeEnter({ next }) {
        // destroy the previous scroll
        scrollCont.destroy();

        // init LocomotiveScroll regarding the next page
        smooth(next.container);
      },
      enter({ next }) {
        pageTransitionOut();
      },
    },
    {
      name: "fill-viewport",
      sync: false,
      from: {
        custom: ({ trigger }) => {
          return (
            trigger.classList && trigger.classList.contains("fill-transition")
          );
        },
      },
      to: {
        namespace: ["experience-show"],
      },
      once({ next }) {
        // init LocomotiveScroll on page load
        smooth(next.container);
      },
      leave({ current, trigger }) {
        const doneFilling = this.async();
        fillViewportIn(doneFilling, trigger);
      },
      beforeEnter({ next }) {
        // destroy the previous scroll
        scrollCont.destroy();

        // init LocomotiveScroll regarding the next page
        smooth(next.container);
      },
      enter({ next }) {
        contentAnimationIn(next.container);
      },
    },
  ],
});

function smooth(container) {
  scrollCont = new LocomotiveScroll({
    el: container.querySelector("[data-scroll-container]"),
    smooth: true,
    smoothMobile: true,
    useKeyboard: false,
    lerp: 0.2,
    scrollFromAnywhere: true,
  });
}

export { scrollCont, barba };
