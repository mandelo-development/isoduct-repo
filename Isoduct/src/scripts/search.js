async function search () {

    let searchForm = document.getElementsByClassName('search_form');
    let timeoutHandle;
    for (var i = 0; i < searchForm.length; i++) {
        searchForm[i].addEventListener("keydown", function(e) {
                let thisForm = this;
                if(e.keyCode !== 27 && e.keyCode !== 13) {
                    window.clearTimeout(timeoutHandle);
                    timeoutHandle = window.setTimeout(function(){
                        let inputValue = thisForm.value;
                        if (inputValue  === '') {
                            let resultsWrapper = document.getElementById('search_results');
                            resultsWrapper.innerHTML = '';
                        } else {
                            getSearchResult(inputValue);
                        }
                    }, 300);
                } else if (e.keyCode === 13) {
                    console.log('enter');
                    let inputValue = thisForm.value;
                    window.location.href = '/zoekresultaten?q=' + inputValue + '&show_layout=true';
                }
            }
        );
    }
    function getSearchResult(inputValue) {
        let request_method = 'GET';
        let variables = [];
        let post_url = '/zoekresultaten?q=' + inputValue + '&show_layout=false';
        function ajaxReq() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                alert("Browser does not support XMLHTTP.");
                return false;
            }
        }
        var xmlhttp = ajaxReq();
        xmlhttp.onreadystatechange = variables;
        xmlhttp.open(request_method, post_url, true); // set true for async, false for sync request
        xmlhttp.setRequestHeader('Content-Type', 'application/json');
        xmlhttp.send(variables); // or null, if no parameters are passed
        xmlhttp.onload = function () {
            let resultsWrapper = document.getElementById('search_results');
            resultsWrapper.innerHTML = xmlhttp.response;
        };
        xmlhttp.onerror = function () {
            alert('error');
        };
    }
} export {
    search
}
