// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller,
	Autoplay,
	EffectFade,
	Mousewheel,
} from '../../../config/node_modules/swiper/swiper.esm';
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Autoplay, EffectFade, Mousewheel]);
import {scrollCont} from './scrollContainer';
var $ = require('../../../config/node_modules/jquery');

const experienceCarousel = () => {
	var swiperContainer = $('.slider-container.experiences');
	var swiperSlide = $('.slider-container.experiences .swiper-slide');
	var experience_slider = new Swiper('.slider-container.experiences', {
		init: false,
		autoplay: true,
		spaceBetween: 0,
		slidesPerView: 1,
		direction: 'vertical',
		grabCursor: false,
		loop: false,
		autoHeight: false,
		speed: 1800,
		observeParents: true,
		observer: false,
		watchSlidesProgress: true,
		simulateTouch: false,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true,
		},
		breakpoints: {
			0: {
				allowTouchMove: false,
			},
			989: {
				allowTouchMove: true,
			}
		}
	});

	experience_slider.init();

	var elementHeights = $(experience_slider.slides.find('.experience-slider__item')).map(function() {
		return $(this).height();
	  }).get();
	
	var maxHeight = Math.max.apply(null, elementHeights);

	$(swiperContainer).height(maxHeight);

	setTimeout(function () {
		scrollCont.update();
	}, 10);
	
}

const indexExperienceCarousel = () => {
	var index_experience_slider = new Swiper('.slider-container.index-experiences', {
		init: false,
		autoplay: {
			delay: 4000,
			disableOnInteraction: true,
		},
		direction: 'horizontal',
		grabCursor: true,
		loop: true,
		breakpoints: {
			0: {
				slidesPerView: 1.2,
				spaceBetween: 30,
			},
			767: {
				slidesPerView: 2,
				spaceBetween: 40,
			},
			1366: {
				slidesPerView: 2.3,
				spaceBetween: 46,
			}
		}
	});

	$('.fill-transition').on('click', function() {
		index_experience_slider.autoplay.stop();
	});

	index_experience_slider.init();
}

const uspSlider = () => {
	var usp_slider = new Swiper('.slider-container.usps', {
		init: false,
		autoplay: true,
		direction: 'vertical',
		loop: true,
		grabCursor: false,
		speed: 800,
	});

	usp_slider.init();
}

const tabSlider = (thisContainer) => {
	var tabSliders = thisContainer.querySelectorAll('.slider-container.tabs-slider');
	if (typeof(tabSliders) != 'undefined' && tabSliders != null) {
		tabSliders.forEach((tabSlider) => {

			console.log(tabSlider)

			var tab_attribs_title = $('.slider-container.tabs-slider .data-tab-slide').map(function () {
				return $(this).attr("data-tab-title");
			});

			tabSlider = new Swiper ('.slider-container.tabs-slider', {
				pagination: {
					el: '.slider-container.tabs-slider .swiper-pagination',
					clickable: true,
					renderBullet: function (index, className) {
						return '<div class="' + className + '">' + (tab_attribs_title[index])+ 
						'</div>';
					},
				},
				loop: false,
				speed: 500,
				spaceBetween: 60,
				mousewheel: false,
				allowTouchMove: false,
			});
		});
	}
}

const logoSlider = (thisContainer) => {
	var logoSliders = thisContainer.querySelectorAll('.swiper-container.premium-partners-slider');
	if (typeof(logoSliders) != 'undefined' && logoSliders != null) {
		logoSliders.forEach((logoSlider) => {

			console.log(logoSlider)

			logoSlider = new Swiper (logoSlider, {
				spaceBetween: 30,
				slidesPerView: 5,
				direction: 'horizontal',
				loop: true,
				autoHeight: true,
				centeredSlides: true,
				speed: 1800,
				mousewheel: true,
				allowTouchMove: true,
				grabCursor: true,
				autoplay: {
					delay: 4000,
					disableOnInteraction: false,
				},
				breakpoints: {
					0: {
						slidesPerView: 1.9,
						spaceBetween: 40,
					},
					767: {
						slidesPerView: 2.3,
						spaceBetween: 30,
					},
					1024: {
						slidesPerView: 4,
					}, 
					1330: {
						slidesPerView: 5,
					},
				}
			});
		});
	}
}

const fadeSlider = (thisContainer) => {
	var fadeSliders = thisContainer.querySelectorAll('.swiper-container.fade-items-slider');
	if (typeof(fadeSliders) != 'undefined' && fadeSliders != null) {
		fadeSliders.forEach((fadeSliderItem) => {

			console.log(fadeSliderItem)

			fadeSliderItem = new Swiper (fadeSliderItem, {
				spaceBetween: 0,
				slidesPerView: 1,
				direction: 'horizontal',
				loop: true,
				autoHeight: false,
				centeredSlides: true,
				speed: 1800,
				mousewheel: true,
				allowTouchMove: true,
				grabCursor: true,
				autoplay: {
					delay: 7000,
					disableOnInteraction: false,
				}, 
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
			});
		});
	}
}
export { experienceCarousel, indexExperienceCarousel, uspSlider, tabSlider, logoSlider, fadeSlider }
