var $ = require("../../../config/node_modules/jquery");
import {scrollCont} from './scrollContainer';

const tabs = () => {
    $(document).ready(function() {

        var tabs = $('.tabs');

        if ($(tabs).length) {

            var tabListItems = $('.tabs .tab__item');
            var tabContentParent = $('.tabs__content__text');
            var tabContent = $('.tabs .tabs__content__text__wrapper');
            var activeTabContent = $('.tabs .tabs__content__text__wrapper.active');

            $(tabContent).each(function () {
                $(this).css('height', $(this).outerHeight());
            });

            addHeight(activeTabContent);

            $(tabListItems).each(function (i) {
                
                $(this).on("click", function (e) {
                    e.preventDefault();
                    var activeListItem = $('.tabs .tab__item.active');
                    activeTabContent = $('.tabs .tabs__content__text__wrapper.active')
                    
                    var dataID = $(this).data("id");
                    var element = $('.tabs .tabs__content__text__wrapper[id="' + dataID + '"]');

                    $(activeListItem).removeClass('active');
                    $(activeTabContent).removeClass('active');
                    $(this).addClass('active');
                    $(element).addClass('active');

                    setTimeout(function () {
                        addHeight(element);
                    }, 100);
                    
                });
            });

            function addHeight(activeItem) {
                $(tabContentParent).height($(activeItem).outerHeight());
                setTimeout(function () {
                    scrollCont.update();
                }, 400);
            }

        }

    });
} 

$(".info-pakket").mouseenter(function(){
    scrollCont.stop();
}).mouseleave(function(){
    scrollCont.start();
    scrollCont.update();
});

export { 
    tabs
}